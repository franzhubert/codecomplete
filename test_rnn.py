#!/bin/python3

import tensorflow as tf
from tensorflow.keras import layers
import numpy as np

# get dataset here
text = "some long text"

maxlen = 5 # extract sequences of length maxlen
step = 2
sentences = [] # holds extracted sequences
next_chars = [] # holds the targets
for i in range(0, len(text)-maxlen, step):
    sentences.append(text[i:i+maxlen])
    next_chars.append(text[i+maxlen])

# vectorization
chars = sorted(set(text))
char_indices = dict((char, chars.index(char)) for char in chars)
x = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        x[i, t, char_indices[char]] = 1
    y[i, char_indices[next_chars[i]]] = 1


model = tf.keras.models.Sequential()
model.add(layers.LSTM(128, input_shape=(maxlen, len(chars))));
model.add(layers.Dense(len(chars), activation="softmax"))

model.summary()

model.compile(loss="categorical_crossentropy", optimizer="adam")

model.save("models/testmodel.h5")
