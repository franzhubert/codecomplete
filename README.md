## codeComplete

codeComplete is an AI-driven autocompletion program for predicting c/c++, java, shell, css, html, python and javascript.

It should later be a plugin for Atom, Intellij and Vim.
